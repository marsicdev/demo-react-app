import { Button } from '@material-ui/core'
import React, { useState } from 'react'

import Section from './Section'

function Content() {
    const [showDesc, setShowDesc] = useState(true) // [true, fn()]

    const handleClick = () => {
        setShowDesc((prevValue) => !prevValue)
    }

    // useEffect(function () {

    // })

    // if (!showDesc) {
    //     return <p>Empty</p>
    // }

    return (
        <main>
            <Button
                variant="outlined"
                color="secondary"
                disableRipple
                onClick={handleClick}>
                Primary
            </Button>

            {showDesc && <h3>This is optional text</h3>}
            {showDesc && <Section />}
        </main>
    )
}

export default Content
