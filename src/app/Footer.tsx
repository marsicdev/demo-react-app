// interface FooterProps {

// }

export type FooterProps = {
    displayDate: string | number
    copy: string
    url?: string
    showDescription?: boolean
}

function Footer(props: FooterProps) {
    const { displayDate, url, showDescription } = props

    return (
        <footer>
            <p>
                Copyright - BUS Computers {displayDate} ({props.copy})
            </p>
            {/* {url ? <p>Website: {url}</p> : null} */}
            {url && <p>Website: {url}</p>}
            {showDescription && <p>This is description</p>}
        </footer>
    )
}

export default Footer
