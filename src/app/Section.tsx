import React, { useState, useEffect } from 'react'
import { Box, TextField, Button } from '@material-ui/core'

function Section() {
    const [posts, setPosts] = useState([])
    const [filteredPosts, setFilteredPosts] = useState([])
    const [searchText, setSearchText] = useState('')

    useEffect(() => {
        console.log('mount')

        fetch('https://jsonplaceholder.typicode.com/posts')
            .then((res) => res.json())
            .then((posts) => {
                setPosts(posts)
                setFilteredPosts(posts)
            })
    }, []) // useEffect(fn, [])

    const handleSearchInput = (event: any) => {
        const { value } = event.target

        if (!value) {
            setFilteredPosts(posts)
        }

        setSearchText(value)
    }

    const handleSearch = () => {
        const newPosts = posts.filter((post: any) => {
            return post.title.startsWith(searchText)
        })

        setFilteredPosts(newPosts)
    }

    if (filteredPosts.length === 0) {
        return <h5>Loading... </h5>
    }

    return (
        <div>
            <h5>POSTS {filteredPosts.length}</h5>

            <Box>
                <TextField
                    id="outlined-basic"
                    label="Outlined"
                    variant="outlined"
                    value={searchText}
                    onChange={handleSearchInput}
                />
                <Button
                    variant="outlined"
                    color="secondary"
                    disableRipple
                    onClick={handleSearch}>
                    Search posts
                </Button>
            </Box>

            {filteredPosts.map((post: any) => {
                return <p>{post.title}</p>
            })}
        </div>
    )
}

export default Section
