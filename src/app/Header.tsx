interface HeaderProps {
    title: string
    children?: React.ReactNode
}

function Header(props: HeaderProps) {
    console.log('props', props)
    return (
        <header>
            Header: {props.title}
            {props.children}
        </header>
    )
}

export default Header
