import React from 'react'
import Content from './Content'
import Footer, { FooterProps } from './Footer'
import Header from './Header'

function App() {
    const footerProps: FooterProps = {
        displayDate: new Date().getFullYear(),
        copy: 'This is protected',
        url: 'https://bus.co.rs',
        showDescription: false,
    }

    return (
        <>
            <Header title="This is my title">
                <h2>This is my nice app</h2>
            </Header>
            <Content />
            <Footer {...footerProps} />
            {/* <Footer
                displayDate={new Date().getFullYear()}
                copy="This is footer 2"
                url="https://bus.co.rs"
            /> */}
        </>
    )
}

export default App

// {/* <App>
// <Header title="Title">
//     <Logo />
//     <Menu>
//         <MenuItem>
//             <Text>Home</Text>
//         </MenuItem>
//         <MenuItem>
//             <Text>About</Text>
//         </MenuItem>
//     </Menu>
// </Header>
// <Content>
//     <Title>
//         <Text>POSTS</Text>
//     </Title>
//     <PostsList>
//         <PostItem>
//             <Text>POSTS TITLE</Text>
//         </PostItem>
//         <PostItem>
//             <Text>POSTS TITLE</Text>
//         </PostItem>
//         <PostItem>
//             <Text>POSTS TITLE</Text>
//         </PostItem>
//         <PostItem>
//             <Text>POSTS TITLE</Text>
//         </PostItem>
//     </PostsList>
// </Content>
// </App> */}
